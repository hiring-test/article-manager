#!/usr/bin/env just --justfile
set dotenv-load

# List recipes
default:
    just --list

# Write <key>="<data>" in .env file. (write nothing if <key> already exists)
_set_value_to_env_file key data:
    @grep -qs {{key}} .env || echo '{{key}}="{{data}}"' >> .env

# Add project domain to /etc/hosts
_add_domain_to_hosts:
    @grep -qs article-manager.local /etc/hosts || echo "127.0.0.1\tarticle-manager.local" | sudo tee -a /etc/hosts

_php_exec command:
    docker-compose exec --user=app php-fpm {{command}}

# Launch project
init: down
    @just _set_value_to_env_file GROUP_ID `id -g`
    @just _set_value_to_env_file USER_ID `id -u`
    @just _add_domain_to_hosts
    docker-compose up -d --build
    @just _php_exec 'composer install'
    @just _php_exec 'bin/console doc:sch:up --force --no-interaction'
    @echo "Project 'article manager' is ready!"
    @echo "Go to http://article-manager.local"

# Stop project
down:
    docker-compose down --remove-orphans

# Connect to php container
shell-php:
    @just _php_exec /bin/sh

# Launch php tests
php-tests:
  @just _php_exec "php bin/phpunit"

# Launch psalm analysis
psalm:
    @just _php_exec 'vendor/bin/psalm --no-diff'

# Launch cs fixer fix
cs-fixer-fix:
    @just _php_exec 'vendor/bin/php-cs-fixer fix --verbose'

# Launch all tests
tests:
  -@just php-tests
  -@just psalm
  -@just cs-fixer-fix
