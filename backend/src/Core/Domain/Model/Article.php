<?php

declare(strict_types=1);

namespace App\Core\Domain\Model;

use App\Core\Domain\Enum\ArticleStatusEnum;
use App\Core\Domain\Exception\NotAllowedStatusChangeException;
use DateTimeImmutable;
use Symfony\Component\Uid\Uuid;

final class Article
{
    public function __construct(
        public readonly Uuid $id,
        private string $title,
        private string $content,
        public readonly string $author,
        private ArticleStatusEnum $status,
        private ?DateTimeImmutable $publishedAt,
    ) {
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getPublishedAt(): ?DateTimeImmutable
    {
        return $this->publishedAt;
    }

    public function getStatus(): ArticleStatusEnum
    {
        return $this->status;
    }

    public function published(): self
    {
        if ($this->status === ArticleStatusEnum::DELETED) {
            throw new NotAllowedStatusChangeException($this, ArticleStatusEnum::PUBLISHED);
        }

        $this->status = ArticleStatusEnum::PUBLISHED;
        $this->publishedAt = new DateTimeImmutable();

        return $this;
    }

    public function drafted(): self
    {
        if ($this->status === ArticleStatusEnum::DELETED) {
            throw new NotAllowedStatusChangeException($this, ArticleStatusEnum::DRAFT);
        }

        $this->status = ArticleStatusEnum::DRAFT;

        return $this;
    }

    public function deleted(): self
    {
        $this->status = ArticleStatusEnum::DELETED;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'content' => $this->content,
            'author' => $this->author,
            'status' => $this->status->value,
            'publishedAt' => $this->publishedAt?->format('d/m/Y H:i:s'),
        ];
    }
}
