<?php

declare(strict_types=1);

namespace App\Core\Domain\Exception;

use App\Core\Domain\Enum\ArticleStatusEnum;
use DomainException;
use Symfony\Component\Uid\Uuid;

final class NotAllowedUpdateException extends DomainException
{
    public function __construct(Uuid $id)
    {
        parent::__construct(
            sprintf(
                'Article with id %s must be in status %s for allowed to updated',
                (string) $id,
                ArticleStatusEnum::DRAFT->value,
            ),
        );
    }
}
