<?php

declare(strict_types=1);

namespace App\Core\Domain\Exception;

use App\Core\Domain\Enum\ArticleStatusEnum;
use App\Core\Domain\Model\Article;
use DomainException;

final class NotAllowedStatusChangeException extends DomainException
{
    public function __construct(
        Article $article,
        ArticleStatusEnum $newStatus,
    ) {
        parent::__construct(
            sprintf(
                'Article with id %s with status %s is not allowed to change status to %s',
                (string) $article->id,
                strtolower($article->getStatus()->value),
                strtolower($newStatus->value),
            ),
        );
    }
}
