<?php

namespace App\Core\Domain\Enum;

enum ArticleStatusEnum: string
{
    case PUBLISHED = 'PUBLISHED';
    case DRAFT = 'DRAFT';
    case DELETED = 'DELETED';
}
