<?php

declare(strict_types=1);

namespace App\Core\Application\Service;

use App\Core\Domain\Enum\ArticleStatusEnum;
use App\Core\Domain\Model\Article;

interface ArticleCreatorServiceInterface
{
    public function create(string $title, string $content, ArticleStatusEnum $status): Article;
}
