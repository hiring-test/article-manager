<?php

declare(strict_types=1);

namespace App\Core\Application\Service;

use App\Core\Application\Repository\ArticleRepositoryInterface;
use App\Core\Domain\Enum\ArticleStatusEnum;
use App\Core\Domain\Exception\NotAllowedUpdateException;
use App\Core\Domain\Model\Article;
use DomainException;
use Symfony\Component\Uid\Uuid;

final readonly class ArticleUpdaterService implements ArticleUpdaterServiceInterface
{
    public function __construct(
        private ArticleRepositoryInterface $articleRepository,
    ) {
    }

    public function update(Uuid $id, string $title, string $content): Article
    {
        $article = $this->articleRepository->getArticleById($id);
        if ($article === null) {
            throw new DomainException(sprintf('Article with id %s not found', (string) $id));
        }

        if ($article->getStatus() == ArticleStatusEnum::PUBLISHED) {
            throw new NotAllowedUpdateException($id);
        }

        $this->articleRepository->update($article->setTitle($title)->setContent($content));

        return $article;
    }
}
