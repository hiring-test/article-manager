<?php

declare(strict_types=1);

namespace App\Core\Application\Service;

use App\Core\Application\Repository\ArticleRepositoryInterface;
use App\Core\Domain\Model\Article;
use DomainException;
use Symfony\Component\Uid\Uuid;

final readonly class ArticleChangeStatusService implements ArticleChangeStatusServiceInterface
{
    public function __construct(
        private ArticleRepositoryInterface $articleRepository,
    ) {
    }

    public function published(Uuid $id): Article
    {
        $article = $this->getArticleById($id);
        $this->articleRepository->update($article->published());

        return $article;
    }

    public function drafted(Uuid $id): Article
    {
        $article = $this->getArticleById($id);
        $this->articleRepository->update($article->drafted());

        return $article;
    }

    public function delete(Uuid $id): Article
    {
        $article = $this->getArticleById($id);
        $this->articleRepository->update($article->deleted());

        return $article;
    }

    private function getArticleById(Uuid $id): Article
    {
        $article = $this->articleRepository->getArticleById($id);
        if ($article === null) {
            throw new DomainException(sprintf('Article with id %s not found', (string) $id));
        }

        return $article;
    }
}
