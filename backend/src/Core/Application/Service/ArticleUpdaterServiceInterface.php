<?php

declare(strict_types=1);

namespace App\Core\Application\Service;

use App\Core\Domain\Model\Article;
use Symfony\Component\Uid\Uuid;

interface ArticleUpdaterServiceInterface
{
    public function update(Uuid $id, string $title, string $content): Article;
}
