<?php

declare(strict_types=1);

namespace App\Core\Application\Service;

use App\Core\Application\Repository\ArticleRepositoryInterface;
use App\Core\Domain\Enum\ArticleStatusEnum;
use App\Core\Domain\Model\Article;
use DateTimeImmutable;
use DomainException;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Uid\Uuid;

final readonly class ArticleCreatorService implements ArticleCreatorServiceInterface
{
    public function __construct(
        private ArticleRepositoryInterface $articleRepository,
        private Security $security,
    ) {
    }

    public function create(string $title, string $content, ArticleStatusEnum $status): Article
    {
        $user = $this->security->getUser();
        if ($user === null) {
            throw new DomainException('User not found');
        }
        $article = new Article(
            Uuid::v4(),
            $title,
            $content,
            $user->getUserIdentifier(),
            $status,
            $status === ArticleStatusEnum::PUBLISHED ? new DateTimeImmutable() : null,
        );

        $this->articleRepository->create($article);

        return $article;
    }
}
