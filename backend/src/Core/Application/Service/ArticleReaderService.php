<?php

declare(strict_types=1);

namespace App\Core\Application\Service;

use App\Core\Application\Repository\ArticleRepositoryInterface;

final readonly class ArticleReaderService implements ArticleReaderServiceInterface
{
    public function __construct(
        private ArticleRepositoryInterface $articleRepository,
    ) {
    }

    public function getArticles(): array
    {
        return $this->articleRepository->getAll();
    }
}
