<?php

declare(strict_types=1);

namespace App\Core\Application\Service;

use App\Core\Domain\Model\Article;

interface ArticleReaderServiceInterface
{
    /** @return Article[] */
    public function getArticles(): array;
}
