<?php

declare(strict_types=1);

namespace App\Core\Application\Service;

use App\Core\Domain\Model\Article;
use Symfony\Component\Uid\Uuid;

interface ArticleChangeStatusServiceInterface
{
    public function published(Uuid $id): Article;

    public function drafted(Uuid $id): Article;

    public function delete(Uuid $id): Article;
}
