<?php

declare(strict_types=1);

namespace App\Core\Application\Repository;

use App\Core\Domain\Model\Article;
use Symfony\Component\Uid\Uuid;

interface ArticleRepositoryInterface
{
    public function create(Article $article): void;

    public function update(Article $article): void;

    public function getArticleById(Uuid $id): ?Article;

    /** @return Article[] */
    public function getAll(): array;
}
