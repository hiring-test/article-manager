<?php

declare(strict_types=1);

namespace App\Core\Infrastructure\Repository;

use App\Core\Application\Repository\ArticleRepositoryInterface;
use App\Core\Domain\Enum\ArticleStatusEnum;
use App\Core\Domain\Model\Article;
use App\Core\Infrastructure\Entity\ArticleEntity;
use Doctrine\ORM\EntityManagerInterface;
use DomainException;
use Symfony\Component\Uid\Uuid;

final readonly class ArticleRepository implements ArticleRepositoryInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function create(Article $article): void
    {
        $this->save(new ArticleEntity(
            $article->id,
            $article->getTitle(),
            $article->getContent(),
            $article->author,
            $article->getStatus(),
            $article->getPublishedAt(),
        ));
    }

    public function update(Article $article): void
    {
        $entity = $this->getById($article->id);
        if ($entity === null) {
            throw new DomainException(sprintf('%s with id %s not found', Article::class, (string) $article->id));
        }
        $this->save(
            $entity->setTitle($article->getTitle())
                ->setContent($article->getContent()),
        );
    }

    public function getArticleById(Uuid $id): ?Article
    {
        $entity = $this->getById($id);
        if ($entity === null) {
            return null;
        }

        return $entity->getModel();
    }

    public function getAll(): array
    {
        /** @var ArticleEntity[] $result */
        $result = $this->entityManager->createQueryBuilder()
            ->select('a')
            ->from(Article::class, 'a')
            ->andWhere('a.status != :status')
            ->setParameter('status', ArticleStatusEnum::DELETED->value)
            ->getQuery()
            ->getArrayResult();

        return array_map(fn (ArticleEntity $articleEntity) => $articleEntity->getModel(), $result);
    }

    private function getById(Uuid $id): ?ArticleEntity
    {
        /** @var ?ArticleEntity $entity */
        $entity = $this->entityManager->createQueryBuilder()
            ->select('a')
            ->from(Article::class, 'a')
            ->where('a.id = :id')
            ->setParameter('id', $id->jsonSerialize())
            ->andWhere('a.status != :status')
            ->setParameter('status', ArticleStatusEnum::DELETED->value)
            ->getQuery()
            ->getOneOrNullResult();

        return $entity;
    }

    private function save(ArticleEntity $articleEntity): void
    {
        $this->entityManager->persist($articleEntity);
        $this->entityManager->flush();
    }
}
