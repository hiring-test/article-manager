<?php

declare(strict_types=1);

namespace App\Core\Infrastructure\Entity;

use App\Core\Domain\Enum\ArticleStatusEnum;
use App\Core\Domain\Model\Article;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/** @psalm-suppress PropertyNotSetInConstructor */
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(name: 'Article')]
class ArticleEntity
{
    #[ORM\Id]
    #[ORM\Column(type: 'string')]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    private string $id;

    #[ORM\Column(type: 'string', length: 128)]
    private string $title;

    #[ORM\Column(type: 'text')]
    private string $content;

    #[ORM\Column(type: 'string', length: 128)]
    private string $author;

    #[ORM\Column(type: 'string', length: 50)]
    private string $status;

    #[ORM\Column(type: 'datetime_immutable')]
    private ?DateTimeImmutable $publishedAt;

    /** @psalm-suppress  UnusedProperty */
    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeImmutable $createdAt;

    /** @psalm-suppress  UnusedProperty */
    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeImmutable $updatedAt;

    public function __construct(
        Uuid $id,
        string $title,
        string $content,
        string $author,
        ArticleStatusEnum $status,
        ?DateTimeImmutable $publishedAt,
    ) {
        $this->id = $id->jsonSerialize();
        $this->title = $title;
        $this->content = $content;
        $this->author = $author;
        $this->status = $status->value;
        $this->publishedAt = $publishedAt;
    }

    public function getId(): Uuid
    {
        return Uuid::fromString($this->id);
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }

    public function getStatus(): ArticleStatusEnum
    {
        return ArticleStatusEnum::from($this->status);
    }

    public function getPublishedAt(): ?DateTimeImmutable
    {
        return $this->publishedAt;
    }

    /** @psalm-suppress  PossiblyUnusedMethod */
    #[ORM\PrePersist]
    public function onPrePersist(): void
    {
        $now = new DateTimeImmutable();
        $this->createdAt = $now;
        $this->updatedAt = $now;
    }

    /** @psalm-suppress  PossiblyUnusedMethod  */
    #[ORM\PreUpdate]
    public function onPreUpdate(): void
    {
        $now = new DateTimeImmutable();
        $this->updatedAt = $now;
    }

    public function getModel(): Article
    {
        return new Article(
            $this->getId(),
            $this->getTitle(),
            $this->getContent(),
            $this->getAuthor(),
            $this->getStatus(),
            $this->getPublishedAt(),
        );
    }
}
