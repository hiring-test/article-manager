<?php

declare(strict_types=1);

namespace App\UserInterface\Controller;

use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

/** @psalm-suppress PropertyNotSetInConstructor */
final class LoginController extends AbstractController
{
    public function __construct(
        private readonly JWTTokenManagerInterface $JWTTokenManager,
        private readonly Security $security,
    ) {
    }

    #[Route('/api/login', name: 'app_login')]
    public function login(): Response
    {
        $user = $this->security->getUser();
        if ($user === null) {
            throw new AuthenticationException('User not found');
        }

        return $this->json([
            'token' => $this->JWTTokenManager->create($user),
        ]);
    }
}
