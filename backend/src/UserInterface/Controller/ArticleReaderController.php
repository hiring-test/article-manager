<?php

declare(strict_types=1);

namespace App\UserInterface\Controller;

use App\Core\Application\Service\ArticleReaderServiceInterface;
use App\Core\Domain\Model\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/** @psalm-suppress PropertyNotSetInConstructor */
final class ArticleReaderController extends AbstractController
{
    public function __construct(
        private readonly ArticleReaderServiceInterface $articleReaderService,
    ) {
    }

    #[Route('/api/articles', methods: ['GET'])]
    public function getAll(): JsonResponse
    {
        return $this->json(
            array_map(
                fn (Article $article) => $article->toArray(),
                $this->articleReaderService->getArticles(),
            ),
        );
    }
}
