<?php

declare(strict_types=1);

namespace App\UserInterface\Controller;

use App\Core\Application\Service\ArticleChangeStatusServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

/** @psalm-suppress PropertyNotSetInConstructor */
final class ArticleChangeStatusController extends AbstractController
{
    public function __construct(
        private readonly ArticleChangeStatusServiceInterface $articleChangeStatusService,
    ) {
    }

    #[Route('/api/article/{id}/published', methods: ['PATCH'])]
    public function published(string $id): JsonResponse
    {
        return $this->json(
            $this->articleChangeStatusService->published(
                Uuid::fromString($id),
            )->toArray(),
        );
    }

    #[Route('/api/article/{id}/drafted', methods: ['PATCH'])]
    public function drafted(string $id): JsonResponse
    {
        return $this->json(
            $this->articleChangeStatusService->drafted(
                Uuid::fromString($id),
            )->toArray(),
        );
    }

    #[Route('/api/article/{id}/deleted', methods: ['DELETE'])]
    public function deleted(string $id): JsonResponse
    {
        return $this->json(
            $this->articleChangeStatusService->delete(
                Uuid::fromString($id),
            )->toArray(),
        );
    }
}
