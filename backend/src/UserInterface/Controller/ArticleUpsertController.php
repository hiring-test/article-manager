<?php

declare(strict_types=1);

namespace App\UserInterface\Controller;

use App\Core\Application\Service\ArticleCreatorServiceInterface;
use App\Core\Application\Service\ArticleUpdaterServiceInterface;
use App\Core\Domain\Enum\ArticleStatusEnum;
use InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

/** @psalm-suppress PropertyNotSetInConstructor */
final class ArticleUpsertController extends AbstractController
{
    private const MAX_TITLE_SIZE = 128;

    public function __construct(
        private readonly ArticleCreatorServiceInterface $articleCreatorService,
        private readonly ArticleUpdaterServiceInterface $articleUpdaterService,
    ) {
    }

    #[Route('/api/article/new', methods: ['POST'])]
    public function create(Request $request): JsonResponse
    {
        $body = json_decode($request->getContent(), true, JSON_THROW_ON_ERROR);
        $this->assertValidBody($body);
        $this->assertValidTitle($body);
        $this->assertValidContent($body);
        $this->assertValidStatus($body);

        $title = $body['title'];
        $content = $body['content'];
        $status = ArticleStatusEnum::from($body['status']);

        return $this->json($this->articleCreatorService->create($title, $content, $status)->toArray());
    }

    #[Route('/api/article/{id}', methods: ['PATCH'])]
    public function update(Request $request, string $id): JsonResponse
    {
        $body = json_decode($request->getContent(), true, JSON_THROW_ON_ERROR);
        $this->assertValidBody($body);
        $this->assertValidTitle($body);
        $this->assertValidContent($body);

        $title = $body['title'];
        $content = $body['content'];

        return $this->json($this->articleUpdaterService->update(Uuid::fromString($id), $title, $content)->toArray());
    }

    /** @psalm-assert array $body */
    private function assertValidBody(mixed $body): void
    {
        if (!is_array($body)) {
            throw new InvalidArgumentException(sprintf('Body must be an array : %s', (string) $body));
        }
    }

    /** @psalm-assert array{'title': string} $body */
    private function assertValidTitle(array $body): void
    {
        $this->assertValidAttribute($body, 'title');
        /** @var string $title */
        $title = $body['title'];
        if (strlen($title) > self::MAX_TITLE_SIZE) {
            throw new InvalidArgumentException(sprintf('Title size must be less than %s', self::MAX_TITLE_SIZE));
        }
    }

    /** @psalm-assert array{'content': string} $body */
    private function assertValidContent(array $body): void
    {
        $this->assertValidAttribute($body, 'content');
    }

    /** @psalm-assert array{'status': string} $body */
    private function assertValidStatus(array $body): void
    {
        $this->assertValidAttribute($body, 'status');
        /** @var string $status */
        $status = $body['status'];
        if ($status === ArticleStatusEnum::DELETED->value) {
            throw new InvalidArgumentException(sprintf('Status must be %s or %s on creation', ArticleStatusEnum::DRAFT->value, ArticleStatusEnum::PUBLISHED->value));
        }
    }

    private function assertValidAttribute(array $data, string $attributeName): void
    {
        if (!array_key_exists($attributeName, $data)) {
            throw new InvalidArgumentException(sprintf('%s is required', $attributeName));
        }

        if (empty($data[$attributeName])) {
            throw new InvalidArgumentException(sprintf('%s can\'t be empty.', $attributeName));
        }

        if (!is_string($data[$attributeName])) {
            throw new InvalidArgumentException(sprintf('%s must be string.', $attributeName));
        }
    }
}
