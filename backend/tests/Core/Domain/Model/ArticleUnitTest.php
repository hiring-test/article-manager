<?php

declare(strict_types=1);

namespace App\Tests\Core\Domain\Model;

use App\Core\Domain\Enum\ArticleStatusEnum;
use App\Core\Domain\Exception\NotAllowedStatusChangeException;
use App\Core\Domain\Model\Article;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;

/** @internal */
final class ArticleUnitTest extends TestCase
{
    /** @test */
    public function itShouldPublishedArticle(): void
    {
        $article = new Article(
            Uuid::v4(),
            'title',
            'content',
            'author',
            ArticleStatusEnum::DRAFT,
            null,
        );

        phpinfo();

        $article->published();

        self::assertEquals(ArticleStatusEnum::PUBLISHED, $article->getStatus());
        self::assertNotNull($article->getPublishedAt());
    }

    /** @test */
    public function itShouldDraftedArticle(): void
    {
        $article = new Article(
            Uuid::v4(),
            'title',
            'content',
            'author',
            ArticleStatusEnum::PUBLISHED,
            new DateTimeImmutable(),
        );

        $article->drafted();

        self::assertEquals(ArticleStatusEnum::DRAFT, $article->getStatus());
    }

    /** @test */
    public function itShouldThrowExceptionOnPublishedWhenIsDeleted(): void
    {
        self::expectException(NotAllowedStatusChangeException::class);
        $article = new Article(
            Uuid::v4(),
            'title',
            'content',
            'author',
            ArticleStatusEnum::DELETED,
            new DateTimeImmutable(),
        );

        $article->published();
    }

    /** @test */
    public function itShouldThrowExceptionOnDraftedWhenIsDeleted(): void
    {
        self::expectException(NotAllowedStatusChangeException::class);
        $article = new Article(
            Uuid::v4(),
            'title',
            'content',
            'author',
            ArticleStatusEnum::DELETED,
            new DateTimeImmutable(),
        );

        $article->drafted();
    }
}
