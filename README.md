# Article Manager

## Requirements

- Linux (not tested for mac or windows !)
- Docker 23.0.1
- Docker-compose 1.27.4
- just (Follow [this link](https://github.com/casey/just) for install on your OS). Small program of the same kind as make

## Installation

```
    just init
```

## Other
    Type following command for show all commands possible.
```
    just
```
